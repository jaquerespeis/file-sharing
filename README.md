# File-sharing systems (_file-sharing_)

Archaeology of file-sharing peer-to-peer systems.

# Table of contents

* [Napster](napster.md)
* [Gnutella](gnutella.md)
* [Freenet](freenet.md)
* [BitTorrent](bittorrent.md)
