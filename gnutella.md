# Anonimity

Gnutella mask the identity of peers that generate a query [^1].

# Measurements

Unless a large percentage of peers collude, it is virtually impossible to find 
the peer that originated a query. 
Thus, it is impossible to measure the number of queries and files downloaded by
each peer. 
[^1]

Possible assumptions [^1]:
* Those that share more also download more, because it is likely that users that 
  share files had to download them.
* Users who have no files are those that will try to access them.

In [^1] they assume that the truth is somewhere between. 

# Free riding



# References

[^1]: Adar, E., & Huberman, B. A. (2000). Free riding on Gnutella. First monday, 5(10), at
https://web.archive.org/web/20190303093140/https://www.hpl.hp.com/research/idl/papers/gnutella/gnutella.pdf
([highlights](https://elopio.keybase.pub/papers/2003-incentives_build_robustness_in_bittorrent.pdf?dl=1))

[2]: Oram, A. (2000). Gnutella and Freenet Represent True
Technological Innovation, The O'Reilly Network, (12 May), at
https://web.archive.org/web/20001213114200/http://www.oreillynet.com/lpt/a/208